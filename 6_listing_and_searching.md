# 6. Listing and searching

## 6.1 Listing commits (showing the history)

### 6.1.1 Branches and files

List all commits on the current branch:  

$ git log  

The most recent commit will be displayed on top.  

For a more compact output use:  

$ git log --oneline  

List commits on a specific branch:  

$ git log <branch>  

List the commits that are on one branch but not the other:  

Given the following:  

```
          F---G---H feature  
         /  
A---B---C---D---E master
```  

The command:  

$ git log feature..master  

Will list commits D and E. This can be useful if you want to know what commits will be included in a merge/rebase.  

List changes to a single file:  

$ git log -- <filename>  

### 6.1.2 Limiting output

To only list the two latest commits:  

$ git log -2  

List commits since a specific date:  

$ git log --since="2019-12-01"

List commits where commit message matches pattern:  

$ git log --grep <pattern>  

### 6.1.3 Pickaxe

The "pickaxe" option. Find commits that references a given string. Can be used to find all commits that references the function printUsage():  

$ git log -S printUsage  

## Exercises

1. Use flag --author to list your own commits.
2. List the latest commit of a specific branch.
3. Use the pickaxe option to find all commits that references some word or function.

## 6.2 Search for content

To search for the occurence of a class, function, variable etc.

$ git grep -n <pattern>

This will display the matching files together with the line number of the match.  
Common grep flags (like -A, -B, -C) can also be used with git grep.

## 6.3 List files in repo

$ git ls-files  

## 6.4 Show revision and author of lines in a file

$ git blame <filename>  

## 6.5 Show contents of a commit

Can be used on commits, branches, tags etc.

$ git show <commit>

List the files included in a commit:  

$ git show --name-only <commit>

## 6.6 Find the commit that introduced some behavior

$ git bisect

This command will help out in searching for a commit that changed some behavior of the code, e.g a bug. You give it two commits, one "good" and one "bad", and git bisect will perform a binary search to find the commit. See '$ git help bisect' for a thorough explanation.

## Exercises

1. Find all files that references some function.
2. Use git log to find the commit hash that has a certain message, e.g UFO-666
3. Use git show to list the changed files in the commit from 2.
