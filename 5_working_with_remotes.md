# 5. Working with remote repositories

## 5.1 Create a remote repository

Start by creating a remote repository. To keep things simple we will create it on the local file system. In the real world, you would access the remote repo on another machine over ssh or https.

$ mkdir ~/git/shared.git  
$ cd ~/git/shared.git  
$ git init --bare  

This will create a bare repository. It will not contain a working tree, only git files. Bare repositories are suffixed with .git by convention.

### 5.2 Add the repository as a remote

Add the repository as a remote:  

$ cd ~/git/cli-workshop  
$ git remote add origin ~/git/shared.git  

The name "origin" can be replaced with whatever you like. 

### 5.3 List remote repositories

$ git remote -v  

This will list all remotes in the current repo together with their url's. Note that you can have several remote repositories and you can add non-bare repositories as well.

### Exercises

1. Use git help to figure out how to rename, change the url, and remove a remote.

## 5.4 Pushing changes to a remote

Push the master branch from ~/git/cli-workshop to the remote repo:  

$ cd ~/git/cli-workshop  
$ git push origin master  

## 5.5 Cloning a repository

$ cd ~/git  
$ git clone ~/git/shared.git  

This will clone the repository into the folder shared. It will add a remote named origin that points to the cloned repository. It will also download and checkout the master branch. 

You should now have two local repositories (~/git/shared and ~/git/cli-workshop) that points to the same remote. 

## 5.6 Download remote changes

When working with a remote repository you will have remote branches as well as your local branches.  

The following will fetch all remote branches from the remote named origin:  

$ git fetch origin  

Note that the remote copy of a branch might not point to the same commit as your local copy. This happens for one of two reasons:  

1. You have made local changes that are not yet pushed.  
2. Someone else have made changes to the remote branch and you have not yet merged them.  

In the first scenario you need to push the changes made to your local branch (see 5.4).  
In the second scenario you need to merge the remote branch to your local branch:  

$ git merge origin/feature

This will merge the remote branch feature to your current branch.  
A handy alternative to fetch + merge is using git pull:  

$ git pull origin feature  

Or if you want to rebase onto the remote branch instead of merging:  

$ git pull origin feature --rebase  

If the local branch has a remote tracking branch set (see next section), then you can omit the remote name and the branch name and simply do:  

$ git pull  

As well as:

$ git push  

## 5.7 Remote branches

### 5.7.1 List remote branches

$ git branch -r 

### 5.7.2 Show tracking info for branches

$ git branch -vv  

This will show the local branches along with the tracking upstream branch.

### 5.7.3 Set tracking branch

To track current branch with upstream branch feature on the remote named origin:  

$ git branch -u origin/feature  

### 5.7.4 Push a local branch

Push the branch named feature to the remote named origin:  

$ git push origin feature  

If you want to track the remote branch, use the -u flag:  

$ git push -u origin feature  

### 5.7.5 Delete a remote branch

To delete the remote branch origin/feature

$ git push origin --delete feature  

Note that this will only delete the remote branch and not the local copy. Use git branch -D <branch> to delete the local copy as well.

### 5.7.3 Delete old remote branches

A branch will be "old" if someone else has removed it from the remote.  
Remove old remote branches explicitly from the remote origin:  

$ git remote prune origin  

Remove old remote branches while fetching:  

$ git fetch --prune  

## Exercises

1. Push changes from one repo and use fetch + merge to get the changes in the other repo.
2. Push some more changes from one repo, but this time set up tracking of the remote branch so you can use git push from one repo and git pull from the other.
3. Delete a remote branch.

## 5.8 Tags

Tags are pointers to commits (just like branches). They are typically used to refer to specific releases. Tags can be lightweight or annotated. A lightweight tag only has a name and points to a commit. An annotated tag contains a message, name of tagger, a date and an optional signature.

### 5.8.1 Create tags

Create a lightweight tag on a given commit:  

$ git tag <tagname> [<commit>]  

### 5.8.2 Share tags

Share a single tag:  

$ git push <remote> <tagname>  

Share all (new) tags:  

$ git push <remote> --tags  

To download tags use:

$ git fetch --tags  

