# 3. Branching and merging

## 3.1 Branches are pointers

Branches are pointers. A branch is a pointer to a commit.  
HEAD - Always points to the current commit.  

## 3.2 Listing branches

List all (local) branches:  
$ git branch   
It will show an asterisk in front of the current branch.

## 3.3 Create a branch

Create a branch from the current commit (HEAD):  
$ git branch <name>

Create a branch from a specific commit:  
$ git branch <name> <commit>  

Remember that branch names in git are lowercase by convention.

## 3.4 Switching between branches

Switch to another branch:  
$ git checkout <branch>  

Create and switch to a new branch in one go:  
$ git checkout -b <name> [<commit>]

NOTE: git switch can be used in newer versions of git.

## 3.5 Deleting a branch

Delete branch (locally):  
$ git branch -D <name>

## 3.6 Renaming a branch

Rename a branch:  
$ git branch -m <oldname> <newname>

Do not do this on shared branches.

## Exercises

1. Create a branch from HEAD.  
2. Create a branch from another commit.  
3. Switch between branches.  
4. Delete a branch.  
5. Rename a branch.  

## 3.7 Finding a common ancestor before creating a branch

Useful when you need to base your branch on a commit that is shared by two or more branches.  

$ git merge-base <branch1> <branch2>  

This will give you a commit hash that can then be specified when creating a new branch.

## 3.8 Merging

### 3.8.1 Fast forward merge

A fast forward happens if no changes have been made to the master branch.

```
          D---E feature  
         /   
A---B---C master  

```

$ git checkout master  
$ git merge feature  

Will result in:  

```
A---B---C---D---E master/feature

```

### 3.8.2 Merge commit

A merge commit will be created if both branches have changes.  
          
```
          E---F feature   
         /  
A---B---C---D master  

```
$ git checkout master  
$ git merge feature  

Will result in:  

```
          E---F  feature  
         /     \  
A---B---C---D---G  master  

```
Where G is the merge commit.

### 3.8.3 Squashing multiple commmits into one

This is useful when when you do not want all intermediate commits incorporated into master when merging a feature.  

```
          D---E---F---G feature  
         /  
A---B---C master

```
$ git checkout master  
$ git merge feature --squash  

Will result in:

```
A---B---C---H master/feature   

```
Where H includes commits D,E,F,G.

## Exercises

1. Perform a fast forward merge from a feature branch to master.  
2. Perform a merge that will create a merge commit.
3. Perform a squashing merge from a feature branch to master.

## 3.9 Rebasing

Reapply commits on top of another branch. Use this when you want to update your feature branch with new changes made on master branch.  

```
         F---G---H feature
        /  
A---B---C---D---E master  

```
$ git checkout feature  
$ git rebase master  

Will result in:  

```
                  F'---G'---H' feature  
                 /   
A---B---C---D---E master  

```
Note that the commits on the feature branch now has new commit hashes since the history has changed.

To rebase or not to rebase?

The golden rule is to never rebase a shared branch. E.g master, develop or a feature branch that is shared with someone else. 

## 3.10 Cherry-picking

Useful when you want to apply a single commit to a branch instead of merging everything.  

```
          D---E---F---G feature  
         /   
A---B---C master  

```

$ git checkout master  
$ git cherry-pick F

Will result in:

```
          D---E---F---G feature  
         /   
A---B---C---F' master  

```

## Exercises

1. Rebase a feature branch on the changes from master.
2. Cherry-pick a commit from a feature branch to master.

## 3.11 Resolving conflicts

Remember that git status is your friendly guide? It will tell you what steps you need to perform.  

$ git status  

It will tell you that you need to resolve the conflict(s), add the file(s) and then commit.

### 3.11.1 Resolving manually

Open the file and search for the merge markers (<<<, >>>, ===)

```
int a = 40;
int b = 2;
<<<<<<< HEAD
int meaningOfLife = a + b;
=======
int meaningOfLife = 42;
>>>>>>> feature
```

Resolve the conflict by choosing the correct version and removing the merge markers.  

```
int a = 40;
int b = 2;
int meaningOfLife = a + b;
```

### 3.11.2 Using a tool

A tool will visualize the conflict(s). Often using three windows: 
BASE, LOCAL, REMOTE.  

LOCAL is where you are standing (HEAD). REMOTE is where you are merging from. BASE is the common ancestor ($ git merge-base LOCAL REMOTE).  

A merge tool will take care of removing the merge markers and adding the file(s).  

Example use with meld:  

$ git config --global merge.tool meld  
$ git mergetool

There are a bunch of tools to try out: kdiff3, meld, bc3, vimdiff... to name a few. 

## Exercises

1. Perform a merge and solve conflict manually.
2. Try the same thing but using a tool.
