# 1. Setup and configuration

## 1.1 Installation

Mac: https://www.atlassian.com/git/tutorials/install-git#mac-os-x  
Linux: https://www.atlassian.com/git/tutorials/install-git#linux  
Windows: https://www.atlassian.com/git/tutorials/install-git#windows

## 1.2 Create repository

$ mkdir ~/git/cli-workshop  
$ cd ~/git/cli-workshop  
$ git init

## 1.3 Git configuration

Local, global and system.  
Local overrides global which overrides system.

Showing configuration:  

$ git config --list --show-origin

List individual parameter:  

$ git config user.name

Setting single parameter:  

$ git config --global core.editor vim

Editing local config (inside repo):  

$ git config -e

Editing global config:  

$ git config --global -e

Some important parameters:

user.name 
user.email 
core.editor 
core.pager
merge.tool
diff.tool 

For pager I would recommend using cat (or empty value). This way one can chain commands together more easily, like this: $ git log | grep "UFO-155"

## 1.4 Shell configuration

Shell completion - completes commands and branch names:  
https://github.com/git/git/blob/master/contrib/completion/git-completion.bash  

The following should be enough on Linux with bash:  
$ sudo apt install git bash_completion

Shell prompt with branch name:  
https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh

## 1.5 Aliases

In git:  
  
$ git config --global alias.co checkout  
$ git co commit/branch

In shell (bash):  
 
edit ~/.bash_aliases  

Example content:  

alias gco='git checkout'  
alias gs='git status'  
