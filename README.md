# git command line interface

## 1. Setup and configuration
git init  
git config  

## 2. Making changes
git help  
git status  
git add  
git commit  
git commit --amend  
git mv  
git rm  
git diff  
git difftool  
gitk  

## 3. Branching and merging
git branch  
git checkout (switch)
git merge-base    
git merge  
git rebase  
git cherry-pick  
git mergetool  

## 4. Undoing changes
git stash  
git checkout (restore)
git reset  (restore --staged)
git revert  
git clean  
git reflog  

## 5. Working with remote repositories
git remote  
git clone  
git fetch (--prune)  
git pull  
git push  
git branch (-r/-vv/-u)  
git tag  

## 6. Listing and searching
git log  
git grep  
git ls-files  
git blame  
git show  
git bisect  

