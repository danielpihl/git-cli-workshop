# 4. Undoing changes

## 4.1 Undoing non-staged changes

Undo changes to modified files:  

$ git checkout -- <file/pattern>

Note that git status will tell you how to do this.

NOTE: git restore can be used in newer versions of git.

## 4.2 Undoing staged changes

Undo changes that have been added to the index:  

$ git reset HEAD <file/pattern>

git status will tell you this as well!

NOTE: git restore --staged can be used in newer versions of git.

## 4.3 Go back to a previous commit

There are more use cases for reset. One of them is to go back to a previous commit.  
This is done with:

$ git reset --hard <commit>

Given the following:  

```
A---B---C---D---E feature/HEAD

```

$ git reset --hard C  

This will completely remove commits D and E:  

```
A---B---C feature/HEAD

```

The flag --soft can be used instead.  

$ git reset --soft C  

This will also reset to commit C in the example above, but the changes in D and E will be kept in the index. This can be an alternative to squashing if you do not want to keep the local history.

## Exercises

1. Undo modifications to a file using git checkout.
2. Unstage a file using git reset.
3. Create one or two commits. Reset to a previous commit using both soft and hard reset.

## 4.4 Removing untracked files

This is typically needed when switching between branches and the newer branch has a lot of new (ignored) files not included in the older branch.

Remove untracked/new files:  

$ git clean -df   

This will remove untracked directories as well.  
Tip: Use -n flag to show what will be removed.

## 4.5 Undoing shared/pushed changes or "old" commits

Undoing a commit:  

$ git revert <commit>  

Undoing a shared commit is the same thing, but do not forget to push the changes.  

$ git revert <commit>  
$ git push  

## 4.6 Reflog - The safety net

The reflog will keep track of where the HEAD and branch pointers used to point. This can be useful if you want to go back in time. For instance to undo a rebase onto the wrong branch. Or to regret a hard reset.  

Show the reflog for HEAD:  

$ git reflog  

Show the reflog for a branch:  

$ git reflog <branch>  

Go back to where HEAD pointed to 3 steps ago:  

```
$ git reset --hard HEAD@{3}  
``` 

## 4.7 Stashing changes

Stashing is useful if you are in the middle of something and you need to do something else. E.g switch branch and fix a bug.   

Stash your changes:  

$ git stash [push]

List your stashes:  

$ git stash list  

```
stash@{0}: WIP on master: 09fd621 risotto  
.. 
``` 

Show what is in a stash:   
```
NOTE: replace <stash> with e.g stash@{0}    
```
$ git stash show <stash> -v 

Apply and remove the latest stash:  

$ git stash pop  

Apply a stash without removing it:  

$ git stash apply <stash>  

## Exercises

1. Make some changes and stash them, show the changes and then pop them. 
2. Use git help stash to find out how to include untracked files in a stash. Also check how to include a description with a stash.

