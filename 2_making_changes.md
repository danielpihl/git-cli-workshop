# 2. Making changes

## 2.1 Help me!

Getting help:  

$ git help <command>  

Show the working tree status:  

$ git status  

If one should know a single git command, git status is the one.  
Think of it as your own guide that will tell you what to do next. Use this command all the time!

## 2.2 Adding files

Adding files to the index/staging area (to be included in the next commit):  

$ git add <file or pattern>  
$ git add *.txt  
$ git add <file1> <file2>  

Add all changed files (but not new ones):  

$ git add -u .  

What does the -u stand for? Check for yourself with $ git help add

### Exercises

1. Create some new files and add one or two of them to the index.

## 2.3 Creating a commit

Create a commit with the staged/indexed changes:  

$ git commit  

This will open the editor that is set in your git config. Commit by entering a message and saving.  
One can also use the flag -m followed by a commit message:  

$ git commit -m "an informative message"  

Amending a commit:  
This will let you update the latest commit. Use it for correcting minor stuff for commits that have not yet been pushed.

Make some (minor) changes to a file.  
Add the changes. Amend with:  

$ git commit --amend  

### Exercises

1. Commit the changes you added previously.  
2. Amend the commit with some minor change.

## 2.4 Move/rename files

$ git mv <file> <newfile>  

This is a shorthand for:  

$ mv <file> <newfile>  
$ git add <file> <newfile>

## 2.5 Delete a file

$ git rm <file>

This is a shorthand for:  

$ rm <file>  
$ git add <file>

## 2.6 Show the changes

To show the changes made that are not yet added/indexed:  

$ git diff  

To show the changes that are added/indexed:  

$ git diff --cached  

To use another application for viewing changes:  

$ git config --global diff.tool meld  
$ git difftool  

A built in tool that I use extensively is gitk. Start it from the repo root:  

$ gitk &  

When you change branch or want to view new changes, simply press File -> Reload. Gitk will show the commit history/log, the non-indexed changes and the indexed changes.  

Tip: Use gitk on a single file to view the history/changes to that file only:  

$ gitk -- <file> &  

gitk behaves similarly to the command 'git log' which will be covered later.

### Exercises  

1. Make some changes to an existing file and view the diff with git diff, git difftool and gitk.  
2. The same thing, but this time make some changes and view the staged/indexed changes.
